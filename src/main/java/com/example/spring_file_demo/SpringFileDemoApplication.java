package com.example.spring_file_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringFileDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringFileDemoApplication.class, args);
    }

}
